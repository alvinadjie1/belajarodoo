# -*- coding: utf-8 -*-
"""file purchase order"""
from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning, ValidationError

class PurchaseOrder(models.Model):
    """inherit models purchase.order"""
    _inherit = 'purchase.order'
    
    keterangan = fields.Char()
    contact_person = fields.Many2one('res.partner')
    phone_purchase = fields.Char(related='contact_person.phone')
    cc = fields.Many2many('res.partner')
    is_lunas = fields.Boolean()
    foto = fields.Binary()
    nama_concat = fields.Char()
    nama_concat_function = fields.Char(compute='_compute_name_concat')
    nama_concat_onchange = fields.Char()
    
    def show_user_error(self, nama=''):
        """return user error message"""
        raise UserError(_('make sure partner id and keterangan not empty '+ nama))
    
    def show_warning(self):
        """return user error message"""
        raise Warning(_('ini warning'))
    
    def show_validation_error(self):
        """return user error message"""
        raise ValidationError(_('ini validation error'))
    
    def nama_concat_keterangan(self):
        """concat field name and keterangan"""
        for rec in self:
            if rec.partner_id and rec.keterangan:
                rec.nama_concat = rec.partner_id.name + ' ' + rec.keterangan
            else:
                rec.show_user_error(rec.partner_id.phone)
    
    #contoh compute
    @api.depends('partner_id', 'keterangan')
    def _compute_name_concat(self):
        """compute name concat"""
        for rec in self:
            if rec.partner_id and rec.keterangan:
                rec.nama_concat_function = rec.partner_id.name + ' ' + rec.keterangan
            else:
                rec.nama_concat_function = ''
    
    # contoh onchange
    @api.onchange('partner_id', 'keterangan')
    def _onchange_nama_concat(self):
        """onchange name concat"""
        for rec in self:
            if rec.partner_id and rec.keterangan:
                rec.nama_concat_onchange = rec.partner_id.name + ' ' + rec.keterangan
            else:
                rec.nama_concat_onchange = ''
                
                


    

    

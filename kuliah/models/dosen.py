"""file dosen"""
from odoo import models, fields, api, _

class Dosen(models.Model):
    """new model"""
    _name       = "dosen"
    _rec_name = "nama"

    """field from dosen"""
    nomor_induk = fields.Char()
    nama        = fields.Char()
    
    def concat_nama_nomor(self):
        for rec in self:
            return rec.nama +' '+rec.nomor_induk
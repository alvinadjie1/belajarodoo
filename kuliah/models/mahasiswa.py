"""file mahasiswa"""
from odoo import models, fields, api, _

class Mahasiswa(models.Model):
    """new model"""
    _name   = "mahasiswa"
    _rec_name = "nama"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    """fields from mahasiswa"""
    nik         = fields.Char()
    nama        = fields.Char()
    foto        = fields.Binary()
    jurusan_id  = fields.Many2one('jurusan', required=True)
    dosen_wali  = fields.Many2one('dosen')
    jadwal_ids  = fields.One2many('jadwalkuliah', 'mahasiswa_id')

    @api.model
    def create(self, vals):
        res = super(Mahasiswa, self).create(vals)
        res.nik = res.jurusan_id.sequence_id.next_by_id()
        return res
    
    #ORM
    def cari_ruang(self):
        ruang = self.env['ruangkelas']
        ruang.create({
            'name':'Tes'
        })
        cari_ruang = self.env['ruangkelas'].search([])
        index=0
        for data in cari_ruang:
            index+=1
            data.write({
                'name':data.name+str(index)
            })
"""file jurusan"""
from odoo import models, fields, api, _

class Jurusan(models.Model):
    """new model"""
    _name = "jurusan"
    _rec_name = "nama_jurusan"

    """fields from jurusan"""
    kode_jurusan = fields.Char(required=True)
    nama_jurusan = fields.Char()
    sequence_id = fields.Many2one('ir.sequence')

    @api.model
    def create(self, vals):
        res = super(Jurusan, self).create(vals)
        res.sequence_id = self.env['ir.sequence'].create({
            'name': 'Jurusan '+res.kode_jurusan,
            'code': res.kode_jurusan,
            'prefix': res.kode_jurusan+'/%(year)s/',
            'padding': '6'
        })
        return res
    
    def write(self, vals):
        res = super(Jurusan, self).write(vals)
        name = self.nama_jurusan
        if 'nama_jurusan' in vals:
            name = vals['nama_jurusan']
        if 'kode_jurusan' in vals:
            new_prefix = vals['kode_jurusan']+'/%(year)s/'
            self.sequence_id.write({
                'prefix':new_prefix,
                'name':name
            })
        return res
# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Belajar Model Kuliah',
    'version': '1.2',
    'category': 'Custom',
    'summary': 'Custom Module',
    'description': "Belajar Membuat Model",
    'website': 'https://www.odoo.com/',
    'author': 'Niko Kusdiarto',
    'depends': [],
    'data': [
        'data/sequence_data.xml',
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/dosen_views.xml',
        'views/ruang_views.xml',
        'views/jurusan_views.xml',
        'views/mata_kuliah_views.xml',
        'views/jadwal_kuliah_views.xml',
        'views/mahasiswa_views.xml',
        'wizard/jurusan_wizard_views.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}

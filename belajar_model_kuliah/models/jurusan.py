"""file jurusan"""
from odoo import models, fields, api, _

class Jurusan(models.Model):
    """new model"""
    _name = "jurusan"
    _rec_name = "nama_jurusan"

    """fields from jurusan"""
    kode_jurusan = fields.Char()
    nama_jurusan = fields.Char()

    @api.model
    def create(self, vals):
        """inherit create to add nik automaticly based on sequence"""
        res = super(Jurusan, self).create(vals)
        #buat sequence baru
        return res
    
    def show_jurusan_wizard(self):
        form_id = self.env.ref('belajar_model_kuliah.view_jurusan_wizard').id
        return {'name': 'Jurusan Wizard',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'jurusan.wizard',
                'views': [(form_id, 'form')],
                'target': 'new',
                'context': {'default_jurusan_id': self.id},
                }
        # return {'name': 'Create MO',
        #         'type': 'ir.actions.act_window',
        #         'view_type': 'form',
        #         'view_mode': 'form',
        #         'res_model': 'wizard.create.mo',
        #         'views': [(self.env.ref('ajh_project.wizard_create_mo_view_form').id, 'form')],
        #         'target': 'new'
        #         }
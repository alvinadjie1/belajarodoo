"""file mahasiswa"""
from odoo import models, fields, api, _

class Mahasiswa(models.Model):
    """new model"""
    _name   = "mahasiswa"
    _rec_name = "nama"

    """fields from mahasiswa"""
    nik         = fields.Char(readonly=True)
    nama        = fields.Char()
    foto        = fields.Binary()
    jurusan_id  = fields.Many2one('jurusan', required=True)
    dosen_wali  = fields.Many2one('dosen')
    jadwal_ids  = fields.One2many('jadwalkuliah', 'mahasiswa_id')
    
    @api.model
    def create(self, vals):
        """inherit create to add nik automaticly based on sequence"""
        res = super(Mahasiswa, self).create(vals)
        sequence_nik = self.env.ref('belajar_model_kuliah.seq_nik')
        res.nik = res.jurusan_id.kode_jurusan + '/' + sequence_nik.next_by_id()
        return res
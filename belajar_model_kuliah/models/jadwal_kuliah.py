"""file jadwal kuliah"""
from odoo import models, fields, api, _

class JadwalKuliah(models.Model):
    """new model"""
    _name = "jadwalkuliah" 
    _rec_name = 'combinationRecName' 

    """field from jadwal kuliah"""
    mahasiswa_id        = fields.Many2one('mahasiswa')
    mata_kuliah         = fields.Many2one('matakuliah')
    hari                = fields.Selection([('senin', 'Senin'), ('selasa', 'Selasa'), ('rabu', 'Rabu'), ('kamis', 'Kamis'), ('jumat', 'Jumat'), ('sabtu', 'Sabtu'), ('minggu', 'Minggu')], string="Hari", default="senin")
    jam                 = fields.Float()
    dosen_id            = fields.Many2one('dosen')
    ruang_id            = fields.Many2one('ruangkelas') 
    combinationRecName  = fields.Char(compute='_compute_field_combination')
    user_id = fields.Many2one('res.users')

    @api.depends('mahasiswa_id', 'mata_kuliah')
    def _compute_field_combination(self):
        for rec in self:
            rec.combinationRecName = rec.mahasiswa_id.nama + ' (' + rec.mata_kuliah.nama + ')'
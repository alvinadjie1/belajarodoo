# -*- coding: utf-8 -*-
"""file product template"""
from odoo import models, fields, api, _
from odoo.exceptions import UserError

class Studio(models.Model):
    """new model studio"""
    _name = 'studio'
    
    name = fields.Char()
    pj = fields.Many2one('hr.employee')
    jadwal_ids = fields.One2many('jadwal', 'studio_id')
    
    
class Jadwal(models.Model):
    """new model jadwal"""
    _name = 'jadwal'
    
    studio_id = fields.Many2one('studio')
    film_id = fields.Many2one('film')
    jam_tayang = fields.Datetime()
    

    

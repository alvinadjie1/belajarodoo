# -*- coding: utf-8 -*-
"""file product template"""
from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning
import random


class Film(models.Model):
    """new model film"""
    _name = 'film'
    
    kode = fields.Char()
    name = fields.Char()
    foto = fields.Binary()
    deskripsi = fields.Text()
    is_manager = fields.Boolean(compute='_compute_is_manager')
    
    def _compute_is_manager(self):
        for rec in self:
            user = self.env.user
            rec.is_manager=False
            if user.has_group('purchase.group_purchase_manager'):
                rec.is_manager = True       
    
    @api.model
    def create(self, vals):
        """inherit create"""
        # assign lewat variable
        vals['kode'] = random.randint(3, 100)
        res = super(Film, self).create(vals)
        #tembak lewat objek
        # res.kode = random.randint(3, 100)
        return res
    
    def write(self, vals):
        """inherit write"""
        # write ketika di inherit bentuknya variable
        res = super(Film, self).write(vals)
        if not self.kode:
            self.kode = random.randint(3, 100)
        return res
    
    def copy(self, default=None):
        self.ensure_one()
        raise Warning('You Cant duplicate this record')
        res = super(Film, self).copy(default)
        return res
    
    def unlink(self):
        user = self.env.user
        if not user.has_group('purchase.group_purchase_manager'):
            raise Warning('You dont have access right to delete this record')
        return super(Film, self).unlink()
    

    

# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Film',
    'version': '13.0.1.0.0',
    'category': 'Custom',
    'summary': 'Film.',
    'description': """
            Module Film
    """,
    'website': 'https://www.portcities.net',
    'author':'Alvin Adji.',
    'images': [],
    'depends': ['hr'],
    'data': [
        'security/ir.model.access.csv',
        'views/film_views.xml',
        'views/studio_views.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True
}
